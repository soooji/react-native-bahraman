import React, { Component } from 'react'
import {StyleSheet, Text, View,TouchableOpacity,Dimensions} from 'react-native'
const WIN = Dimensions.get('window')
import Icon from 'react-native-vector-icons/FontAwesome';
import RF from 'react-native-responsive-fontsize'

export default class TabBarComponentBahraman extends Component {
    
  render() {
    return (
      <View style={[styles.tabBar,styles.FLEX_RR_N_SB_C_S]}>
        <TouchableOpacity onPress={()=>this.props.navigation.navigate("zaferan")} activeOpacity={.8} style={styles.tabBarItem}>
            <Text style={[styles.tabBarIcon,{color:this.props.navigation.state.index == 0 ? 'rgb(203,168,95)' :"#959595"}]}>زعفران</Text>        
        </TouchableOpacity>
        <View style={styles.spacer}></View>
        <TouchableOpacity onPress={()=>this.props.navigation.navigate("nabat")} activeOpacity={.8} style={styles.tabBarItem}>
            <Text style={[styles.tabBarIcon,{color:this.props.navigation.state.index == 1 ? 'rgb(203,168,95)' :"#959595"}]}>نبات</Text>        
        </TouchableOpacity>
        <View style={styles.spacer}></View>
        <TouchableOpacity onPress={()=>this.props.navigation.navigate("advie")} activeOpacity={.8} style={styles.tabBarItem}>
            <Text style={[styles.tabBarIcon,{color:this.props.navigation.state.index == 2 ? 'rgb(203,168,95)' :"#959595"}]}>ادویه</Text>        
        </TouchableOpacity>
        <View style={styles.spacer}></View>
        <TouchableOpacity onPress={()=>this.props.navigation.navigate("other")} activeOpacity={.8} style={styles.tabBarItem}>
            <Text style={[styles.tabBarIcon,{color:this.props.navigation.state.index == 3 ? 'rgb(203,168,95)' :"#959595"}]}>دیگرمحصولات</Text>
        </TouchableOpacity>
      </View>
    )
  }
}
const styles = StyleSheet.create({
    spacer: {
        height:'50%',
        backgroundColor:'#eeeeee',
        width:2,
        borderRadius:10
    },
    tabBarIcon: {
        fontSize: RF(1.8),
        textAlign:'center'
    },
    tabBarItem: {
        paddingVertical:10,
        width: (WIN.width-20)/4
    },
    tabBar: {
        backgroundColor:'rgb(255, 255, 255)',
        width:'100%',
        shadowRadius:20,
        shadowOpacity:.4,
        shadowColor:'#b7b7b7',
        shadowOffset:{width:0,height:-14},
        paddingBottom:0
    },
    FLEX_RR_N_SB_C_S :{
        display: 'flex',
        flexDirection: 'row-reverse',
        flexWrap: 'nowrap',
        justifyContent: 'space-between',
        alignItems: 'center',
        alignContent: 'stretch',
    }
})