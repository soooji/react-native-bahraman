import React, { Component } from 'react'
import {StyleSheet,ImageBackground,Text, View,TouchableOpacity,Dimensions,Image} from 'react-native'
import { createStackNavigator, createAppContainer ,createSwitchNavigator} from "react-navigation";
const WIN = Dimensions.get('window')
import Icon from 'react-native-vector-icons/FontAwesome';
import RF from 'react-native-responsive-fontsize'
import AutoHeightImage from 'react-native-auto-height-image';

import DrawerComponent from './../../components/Main/Drawer'
import Drawer from 'react-native-drawer'
// import Govahiname from './Govahiname'
// import AboutBahraman from './AboutBahraman'
// import Products from './Products'
// import Call from './Call'
// import News from './News'
// import Quality from './Quality'

// const appnavigator=createSwitchNavigator({
//     govahiname : {screen : Govahiname},
//     aboutBahraman :{screen:AboutBahraman },
//     products :{screen:Products,
//     },
//     call:{screen :Call},
//     news :{screen :News},
//     quality :{screen:Quality}
    
//   })

const BahramanBG = require("./../../static/images/bahraman-bg.png");
const BahramanLOGO = require("./../../static/images/bahraman-logo.png");
const IconProducts = require("./../../static/images/icon-products.png");
const IconInfo = require("./../../static/images/icon-info.png");
const IconCertificate = require("./../../static/images/icon-certificate.png");
const IconTick = require("./../../static/images/icon-tick.png");
const IconNews = require("./../../static/images/icon-news.png");
const IconContact = require("./../../static/images/icon-contact.png");
// const navigation=this.props.navigation
export default class BahramanScreen extends Component {
    
    constructor(props) {
        super(props);
        this.state = {
            isDrawerOpen: false
        }
    }
    
      closeControlPanel = () => {
        this._drawer.close()
      };
      onOpenDrawer() {
        this.props.navigation.setParams({isDrawerOpen: true })
      }
      onCloseDrawer() {
        this.props.navigation.setParams({isDrawerOpen: false })
    }
      openControlPanel = () => {
        this._drawer.open()
      };
      componentDidMount() {
        this.props.navigation.setParams({ openDrawer: ()=>this._drawer.open(),closeDrawer: ()=>this._drawer.close(), isDrawerOpen: false })
      }
  render() {
    return (
        <Drawer
        navigation={this.props.navigation}
        side={"right"}
        ref={(ref) => this._drawer = ref}
        type="overlay"
        content={<DrawerComponent/>}
        tapToClose={true}
        onOpen={()=>this.onOpenDrawer()}
        onClose={()=>this.onCloseDrawer()}
        openDrawerOffset={0.2} // 20% gap on the left side of drawer
        panCloseMask={0.2}
        closedDrawerOffset={-3}
        styles={drawerStyles}
        tweenHandler={(ratio) => ({
            main: { opacity:(2-ratio)/2 }
        })}
        >
        <View style={styles.container}>
            <ImageBackground source={BahramanBG} resizeMode={"cover"} style={styles.BahramanBg}>
                <View style={[styles.BorderedBox,styles.FLEX_C_NW_FE_C_S]}>
                    <AutoHeightImage width={WIN.width/2.3} source={BahramanLOGO}/>
                    <View style={[styles.MenuBox,styles.FLEX_RR_W_SB_C_S]}>

                        <TouchableOpacity onPress={() => this.props.navigation.navigate('products')} style={styles.MenuItem}>
                            <Image source={IconProducts} style={styles.MenuImage} resizeMode={"contain"}/>
                            <Text style={styles.MenuTitle}>محصولات</Text>
                        </TouchableOpacity>

                        <TouchableOpacity onPress={() => this.props.navigation.navigate('aboutBahraman')} style={styles.MenuItem}>
                            <Image source={IconInfo} style={styles.MenuImage} resizeMode={"contain"}/>
                            <Text style={styles.MenuTitle}>درباره بهرامن</Text>
                        </TouchableOpacity>

                        <TouchableOpacity onPress={() => this.props.navigation.navigate('Govahiname')} style={styles.MenuItem}>
                            <Image source={IconCertificate} style={styles.MenuImage} resizeMode={"contain"}/>
                            <Text style={styles.MenuTitle}>گواهینامه ها</Text>
                        </TouchableOpacity>

                        <TouchableOpacity onPress={() => this.props.navigation.navigate('quality')} style={styles.MenuItem}>
                            <Image source={IconTick} style={styles.MenuImage} resizeMode={"contain"}/>
                            <Text style={styles.MenuTitle}>کنترل کیفیت</Text>
                        </TouchableOpacity>

                        <TouchableOpacity onPress={() => this.props.navigation.navigate('news')} style={styles.MenuItem}>
                            <Image source={IconNews} style={styles.MenuImage} resizeMode={"contain"}/>
                            <Text style={styles.MenuTitle}>اخبار</Text>
                        </TouchableOpacity>

                        <TouchableOpacity onPress={() => this.props.navigation.navigate('call')} style={styles.MenuItem}>
                            <Image source={IconContact} style={styles.MenuImage} resizeMode={"contain"}/>
                            <Text style={styles.MenuTitle}>تماس با ما</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </ImageBackground>
        </View>
    </Drawer>
    )
  }
}
const drawerStyles = {
    drawer: {},
    main: {paddingLeft: 3},
}
const styles = StyleSheet.create({
    MenuImage :{
        height: 35
    },
    MenuTitle :{
        textAlign:'center',
        fontSize:RF(2.7),
        // fontFamily: "Mj_Saadi",
        color: '#a7a7a7',
        width:'100%'
    },
    MenuItem : {
        width:"30%",
        justifyContent:'center',
        flexDirection:'column',
        alignItems:'center',
        marginBottom:15,
        padding:"1%"
    },
    MenuBox : {
        marginTop:30,
        width:"100%"
    },
    BorderedBox:{
        borderWidth:3,
        borderColor:'white',
        margin:25,
        height:'100%',
        paddingBottom:60
    },
    BahramanBg:{
        width: '100%',
        height: '100%',
    },
    container: {
        flex: 1,
        backgroundColor:'white'
    },
    FLEX_C_NW_FE_C_S : {
        display: 'flex',
	    flexDirection: 'column',
	    flexWrap: 'nowrap',
	    justifyContent: 'flex-end',
	    alignItems: 'center',
	    alignContent: 'stretch',
    },
    FLEX_RR_W_SB_C_S :{
        display: 'flex',
        flexDirection: 'row-reverse',
        flexWrap: 'wrap',
        justifyContent: 'space-between',
        alignItems: 'center',
        alignContent: 'stretch',
    }
})
// export default createAppContainer(BahramanScreen,appnavigator);