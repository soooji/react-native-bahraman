import React, { Component } from 'react';
import {View ,Text,Image,Modal,ScrollView,StyleSheet,Dimensions,ImageBackground,Platform,StatusBar} from 'react-native'
import Drawer from 'react-native-drawer'
import DrawerComponent from './../../../components/Main/Drawer'
// import TabBarComponent from './../../components/Main/TabBarComponent'
// import AutoHeightImage from 'react-native-auto-height-image';
// const govahoname_bahraman=require('./../../../static/images/tabs/bahraman-govahinameha.png')
import Product from './../Product'
import AutoHeightImage from 'react-native-auto-height-image';
import ProductDetail from './../ProductDetail'
let width = Dimensions.get('window').width
let height = Dimensions.get('window').height
const url = require('./../../../static/images/mahsoolat/bahraman-mahsoolat-detail-withoutHeader.png')
const header=require('./../../../static/images/mahsoolat/islamic-top-line.png')

class Zaferan extends Component {
  
    

    constructor(props) {
        super(props);
        this.state = {
            isDrawerOpen: false,
            modalVisible: false,
        }
    }
      setModalVisible(visible) {
      this.setState({modalVisible: visible});
      }
    
      closeControlPanel = () => {
        this._drawer.close()
      };
      onOpenDrawer() {
        this.props.navigation.setParams({isDrawerOpen: true })
      }
      onCloseDrawer() {
        this.props.navigation.setParams({isDrawerOpen: false })
    }
      openControlPanel = () => {
        this._drawer.open()
      };
      componentDidMount() {
        this.props.navigation.setParams({ openDrawer: ()=>this._drawer.open(),closeDrawer: ()=>this._drawer.close(), isDrawerOpen: false })
      }
    render() {
        return (
        <Drawer
            side={"right"}
            ref={(ref) => this._drawer = ref}
            type="overlay"
            content={<DrawerComponent/>}
            tapToClose={true}
          
            onOpen={()=>this.onOpenDrawer()}
            onClose={()=>this.onCloseDrawer()}
            openDrawerOffset={0.2} // 20% gap on the left side of drawer
            panCloseMask={0.2}
            closedDrawerOffset={-3}
            styles={drawerStyles}
            tweenHandler={(ratio) => ({
                main: { opacity:(2-ratio)/2 }
            })}
        >
        <AutoHeightImage source={header} style={styles.headerStyle}/>
        <ScrollView>
          
          <View style={styles.container}>
            <Product pic={url} type='زعفران خاتم نگین' weight='گرم۳' weight='3gram'/>
            <Product pic={url} type='زعفران خاتم نگین' weight='گرم۳' weight='3gram'/>
            <Product pic={url} type='زعفران خاتم نگین' weight='گرم۳' weight='3gram'/>
            <Product pic={url} type='زعفران خاتم نگین' weight='گرم۳' weight='3gram'/>
            <Product pic={url} type='زعفران خاتم نگین' weight='گرم۳' weight='3gram'/>
            <Product pic={url} type='زعفران خاتم نگین' weight='گرم۳' weight='3gram'/>
            <Product pic={url} type='زعفران خاتم نگین' weight='گرم۳' weight='3gram'/>
            <Product pic={url} type='زعفران خاتم نگین' weight='گرم۳' weight='3gram'/>
          </View>
        </ScrollView>

         </Drawer>
        );
    }
}
const drawerStyles = {
    drawer: {},
    main: {paddingLeft: 3},
}
const styles = StyleSheet.create({
    scroll:{
      width:'100%',
      height:'100%',
      // backgroundColor:'#5f0055'
      alignItems: 'center',
    },
    container: {
      flex:1,
      // marginTop:20,
      flexDirection:'row',
      alignItems: 'center',
      flexWrap:'wrap',
      justifyContent:'center',
      alignContent:'stretch'

    },
    headerStyle:{
      // backgroundColor:'red',
      alignItems: 'center',
      justifyContent:'center',
      width:'100%',
      height:'10%',
      marginTop:0.1,
      // position:'absolute'
  },

    BahramanBg:{
        width: '100%',
        height: '100%',
    },
      fixed: {
        position: "absolute",
        top: 0,
        left: 0,
        right: 0,
        bottom: 0
      },
})

export default Zaferan;