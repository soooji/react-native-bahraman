import React, { Component } from 'react';
import {View,Text ,Image,StyleSheet,Dimensions,TouchableOpacity} from 'react-native'
import AutoHeightImage from 'react-native-auto-height-image';
import NavigationServices from './../../navigation-service'
let width = Dimensions.get('window').width

class Product extends Component {
    constructor(props){
        super(props);
        this.state={
            // type:this.props.pic,
            // pic:this.props.type,
            // weight:this.props.weight
        }
    }
    render() {
        return (
            <View style={styles.container}>
                <TouchableOpacity onPress={() => NavigationServices.navigate('productDetail' ,
                {pic:this.props.pic ,type:this.props.type,weight:this.props.weight})}>
                    <AutoHeightImage source={this.props.pic} style={styles.picStyle}/>
                    <Text style={styles.namestyle}>{this.props.type}</Text>
                    <Text style={{textAlign:'center',color:'rgb(179, 179, 179)'}}>{this.props.weight}</Text>
                </TouchableOpacity>
            </View>
        );
    }
}
const styles=StyleSheet.create({
    container:{
        backgroundColor:'#F8F8F8',
        width:width*0.4,
        height:width*0.42,
        marginTop:20,
        margin:10,
        borderRadius:7,
        shadowOffset:{  width: 0,  height:0,  },
        shadowColor: '#b7b7b7',
        shadowOpacity: .5,
        // shadowRadius:2,

    },
    picStyle:{
        // marginTop:20,
        alignItems: 'center',
        justifyContent:'center',
        width:width*0.4,
        height:width*0.32,
        // borderTopLeftRadius:20
    },
    namestyle:{
        textAlign:'center',
        color:'rgb(203,168,95)'
    },
    picStyle:{
        // marginTop:10,
        width:width*0.4,
        height:width*0.32,
        borderTopLeftRadius:7
    }

})
export default Product;