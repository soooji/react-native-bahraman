import React, { Component } from 'react';
import {View ,Text,Image,ScrollView,StyleSheet,Dimensions,I18nManager,ImageBackground,Platform,StatusBar} from 'react-native'
import Drawer from 'react-native-drawer'
import DrawerComponent from './../../components/Main/Drawer'
import AutoHeightImage from 'react-native-auto-height-image';// import { ScrollView } from 'react-native-gesture-handler';
const header=require('./../../static/images/tabs/about-pic.png')
const header_bar=require('./../../static/images/tabs/head-bar.png')
let width = Dimensions.get('window').width
let height = Dimensions.get('window').height

class AboutBahraman extends Component {
    constructor(props) {
        super(props);
        I18nManager.forceRTL(false);
        this.state = {
            isDrawerOpen: false
        }
    }
    
      closeControlPanel = () => {
        this._drawer.close()
      };
      onOpenDrawer() {
        this.props.navigation.setParams({isDrawerOpen: true })
      }
      onCloseDrawer() {
        this.props.navigation.setParams({isDrawerOpen: false })
    }
      openControlPanel = () => {
        this._drawer.open()
      };
      componentDidMount() {
        this.props.navigation.setParams({ openDrawer: ()=>this._drawer.open(),closeDrawer: ()=>this._drawer.close(), isDrawerOpen: false })
      }
    render() {
        return (
        <Drawer
            side={"right"}
            ref={(ref) => this._drawer = ref}
            type="overlay"
            content={<DrawerComponent navigation={this.props.navigation}/>}
            tapToClose={true}
            onOpen={()=>this.onOpenDrawer()}
            onClose={()=>this.onCloseDrawer()}
            openDrawerOffset={0.2} // 20% gap on the left side of drawer
            panCloseMask={0.2}
            closedDrawerOffset={-3}
            styles={drawerStyles}
            tweenHandler={(ratio) => ({
                main: { opacity:(2-ratio)/2 }
            })}
        >
        
        <ScrollView contentContainerStyle={{alignItems: 'center'}}>
                <AutoHeightImage source={header_bar} width={width}/>
                <Text>زعفران بهرامن</Text>
                <View sttyle={{marginTop:10}}>
                <Text sttyle={{textAlign: 'justify'}}>شرکت زعفران بهرامن در سال ۱۳۴۹ با نام عباس عباس زاده با نیت با نیت نوآوری در زمینه تولید و بسته بندی زعفران پا به عرصه ی همت گذارده و پس از ۲۴ سال تلاش بی وقفه همگام با گذشت زمان و لا پیشرفت جامعه و تکنولوژی و احساس نیاز به یک نام تجاری انحصاری، در سال ۱۳۷۳ به زعفران بهرامن تغییر نام یافته و با این نام فعالیت خود را را به عنوان یک شرکت دانش محور که مشتری را در مقام شریک استراتژیک خود می شناسد ادامه داده است.
شرکت زعفران بهرامن با بهره گیری از تحارب ارزشمند و طولانی مدت مدیران متخصصین و کارشناسان با تجهیز کامل ازمایشگاه کنترل کیفیت و با انجام ازمایشات لازم در جهت ارتقاع کیفیت محصول همواره مطابق با استاندارد های ملی و بین المللی پا به عرصه ی رقابت گذاشته و توانسته دستاورد ها و افتخاراتی را نصیب خود نماید.</Text>
                </View>
                <AutoHeightImage source={header} width={width}/>
        </ScrollView>
        </Drawer>
        );
    }
}
const drawerStyles = {
    drawer: {},
    main: {paddingLeft: 3},
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor:'white',
        // width:'100%'
    },
    BahramanBg:{
        width: '100%',
        height: '100%',
    },
      fixed: {
        position: "absolute",
        top: 0,
        left: 0,
        right: 0,
        bottom: 0
      },
})

export default AboutBahraman;