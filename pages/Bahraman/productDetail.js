import React, { Component } from 'react';
import {View,Text ,Image,StyleSheet,Dimensions,TouchableOpacity} from 'react-native'
import AutoHeightImage from 'react-native-auto-height-image';
let width = Dimensions.get('window').width
let height = Dimensions.get('window').height
import RF from 'react-native-responsive-fontsize'
import NavigationServices from './../../navigation-service'
const header=require('./../../static/images/mahsoolat/islamic-top-line.png')
const eslimi=require('./../../static/images/mahsoolat/Layer 4.png')



class ProductDetail extends Component {
    
    render() {
    const { navigation } = this.props;
    const pic = navigation.getParam('pic')
    const type=navigation.getParam('type')
    const weight=navigation.getParam('weight')
        return (
            <View style={styles.container}>
            <AutoHeightImage source={header} style={styles.headerStyle}/>
                <AutoHeightImage source={pic} style={styles.picStyle}/>
                <View style={styles.textContainer}>
                    <Text style={styles.namestyle}>{type}</Text>
                </View>
                    <Text style={styles.weightstyle}>{weight}</Text>
                 <AutoHeightImage source={eslimi} style={styles.eslimi}/>
            </View>
        );
    }
    
}
const styles=StyleSheet.create({
    container:{
        width:'100%',
        height:'100%',
        shadowOffset:{  width: 0,  height:0,  },
        shadowColor: '#b7b7b7',
        shadowOpacity: .5,
        top:-20

    },
    picStyle:{
        alignItems: 'center',
        justifyContent:'center',
        width:'100%',
        height:'60%',
    },

    headerStyle:{
        alignItems: 'center',
        justifyContent:'center',
        width:'100%',
        height:'10%',
        marginTop:10
        // position:'absolute'
    },
    namestyle:{
        textAlign:'center',
        color:'#eeeeee',
        fontSize:RF(3.7),
    },
    weightstyle:{
        textAlign:'center',
        color:'rgb(179, 179, 179)',
        fontSize:RF(3.7),
    },
    textContainer:{
        backgroundColor:'rgb(203,168,95)',
        width:'100%',
        height:'10%',
        alignItems: 'center',
        justifyContent:'center',

    },
    eslimi:{
        marginTop:70,
        // backgroundColor:'red',
        // marginBottom:-1,
        // position:abso
        bottom:1,
        width:'100%',
        height:'17%',
    }
})

export default ProductDetail;