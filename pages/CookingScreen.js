import React, { Component } from 'react'
import {StyleSheet,ScrollView,ImageBackground,Text, View,TouchableOpacity,Dimensions,Image} from 'react-native'
const WIN = Dimensions.get('window')
import Icon from 'react-native-vector-icons/FontAwesome';
import RF from 'react-native-responsive-fontsize'
import AutoHeightImage from 'react-native-auto-height-image';

import DrawerComponent from './../components/Main/Drawer'
import Drawer from 'react-native-drawer'

const farangi = require("./../static/images/cooking/ashpazi1.png")

export default class CookingScreen extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            isDrawerOpen: false
        }
    }
    closeControlPanel = () => {
        this._drawer.close()
      };
      onOpenDrawer() {
        this.props.navigation.setParams({isDrawerOpen: true })
      }
      onCloseDrawer() {
        this.props.navigation.setParams({isDrawerOpen: false })
    }
      openControlPanel = () => {
        this._drawer.open()
      };
      componentDidMount() {
        this.props.navigation.setParams({ openDrawer: ()=>this._drawer.open(),closeDrawer: ()=>this._drawer.close(), isDrawerOpen: false })
      }
    render() {
        return (
        <Drawer
        side={"right"}
        ref={(ref) => this._drawer = ref}
        type="overlay"
        content={<DrawerComponent navigation={this.props.navigation}/>}
        tapToClose={true}
        onOpen={()=>this.onOpenDrawer()}
        onClose={()=>this.onCloseDrawer()}
        openDrawerOffset={0.2} // 20% gap on the left side of drawer
        panCloseMask={0.2}
        closedDrawerOffset={-3}
        styles={drawerStyles}
        tweenHandler={(ratio) => ({
            main: { opacity:(2-ratio)/2 }
        })}
        >
        <ScrollView style={styles.container} bounces={false} stickyHeaderIndices={[0]}>
            <View style={[styles.Flex_R_N_SE_B_S,styles.MenuRow]}>
                <TouchableOpacity><AutoHeightImage width={(WIN.width/100*70)/2} source={farangi}/></TouchableOpacity>
                {/* <TouchableOpacity><AutoHeightImage width={(WIN.width/100*70)/2} source={Documents}/></TouchableOpacity> */}
            </View>
        </ScrollView>
        </Drawer> 
        );
    }
}

const drawerStyles = {
    drawer: {},
    main: {paddingLeft: 3},
}
const styles = StyleSheet.create({
    MenuImage :{
        height: 35
    },
    MenuTitle :{
        textAlign:'center',
        fontSize:RF(2.7),
        // fontFamily: "Mj_Saadi",
        color: '#a7a7a7',
        width:'100%'
    },
    MenuItem : {
        width:"30%",
        justifyContent:'center',
        flexDirection:'column',
        alignItems:'center',
        marginBottom:15,
        padding:"1%"
    },
    MenuBox : {
        marginTop:30,
        width:"100%"
    },
    BorderedBox:{
        borderWidth:3,
        borderColor:'white',
        margin:25,
        height:'100%',
        paddingBottom:60
    },
    BahramanBg:{
        width: '100%',
        height: '100%',
    },
    container: {
        flex: 1,
        backgroundColor:'white'
    },
    FLEX_C_NW_FE_C_S : {
        display: 'flex',
	    flexDirection: 'column',
	    flexWrap: 'nowrap',
	    justifyContent: 'flex-end',
	    alignItems: 'center',
	    alignContent: 'stretch',
    },
    FLEX_RR_W_SB_C_S :{
        display: 'flex',
        flexDirection: 'row-reverse',
        flexWrap: 'wrap',
        justifyContent: 'space-between',
        alignItems: 'center',
        alignContent: 'stretch',
    },
    Flex_R_N_SE_B_S: {
        display: 'flex',
        flexDirection: 'row',
        flexWrap: 'nowrap',
        justifyContent: 'space-evenly',
        alignItems: 'baseline',
        alignContent: 'stretch'
    },
    MenuRow :{
        paddingHorizontal:(WIN.width/100*3)/2,paddingTop:10   
    },
})
