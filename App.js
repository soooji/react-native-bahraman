import React from 'react';
import { Text, View, Platform, StyleSheet, StatusBar, Button, TouchableOpacity } from 'react-native';
import { createBottomTabNavigator, createAppContainer,createStackNavigator ,createMaterialTopTabNavigator} from 'react-navigation';
import TabBarComponentBahraman from './components/Main/TabBarComponent'
import Icon from 'react-native-vector-icons/FontAwesome';
import RF from 'react-native-responsive-fontsize'
//pages
import BahramanScreen from './pages/Bahraman/index'
import CookingScreen from './pages/CookingScreen'
import Govahiname from './pages/Bahraman/Govahiname'
import AboutBahraman from './pages/Bahraman/AboutBahraman'
import Call from './pages/Bahraman/Call'
import News from './pages/Bahraman/News'
import Quality from './pages/Bahraman/Quality'
import NavigationService from './navigation-service';
import productTab from './navigators/productTab'
import Galary from './pages/Galary'
import Documents from './pages/Documents'
import Knowledge from './pages/Bahraman/Knowledge'
import Drawer from './components/Main/Drawer'
const BahramanNavigator = createStackNavigator(
  {
      BahramanPage: {screen: BahramanScreen,params: { title: 'بهرامن' }},
      Govahiname : {screen : Govahiname,params: { title: 'درباره ی بهرامن' }},
      aboutBahraman :{screen:AboutBahraman,params: { title: 'درباره ی بهرامن' } },
      products :{screen:productTab,params: { title: 'محصولات بهرامن' }},
      call:{screen :Call,params: { title: 'بهرامن' }},
      news :{screen :News,params: { title: 'بهرامن' }},
      quality :{screen:Quality,params: { title: 'بهرامن' }},
      knowledge :{screen:Knowledge,params: { title: 'شناسایی انواع زعفران' }},


    },
  {
    initialRouteName: 'BahramanPage',
    defaultNavigationOptions : {
      header : props => <View style={[{position:'relative',backgroundColor:'white'},styles.topBar]}><Text style={[{color:'#969696'},styles.topBarText]}>
      {props.scene.route.params.title}
      </Text>
      <TouchableOpacity activeOpacity={.6} style={{position:'absolute',top:45,right:20}}
      onPress={()=> props.scene.route.params.isDrawerOpen ? props.scene.route.params.closeDrawer() : props.scene.route.params.openDrawer()}>
        <Icon name={props.scene.route.params.isDrawerOpen ? "times" : "bars"} color={"#aaaaaa"} size={RF(2.5)}/>
      </TouchableOpacity>
      <TouchableOpacity activeOpacity={.6} style={{position:'absolute',top:45,left:20}}
      onPress={()=>props.navigation.navigate('BahramanPage')}>
        <Icon name='chevron-left' color={"#aaaaaa"} size={RF(2.5)}/>
      </TouchableOpacity>
      </View>
    }
  },
  
      
);
 const CookingNavigator = createStackNavigator(
  {
    CookingPage: {screen: CookingScreen,params: { title: 'اشپزی' }},
  },
  {
    initialRouteName: 'CookingPage',
    defaultNavigationOptions : {
      header : props => <View style={[{position:'relative',backgroundColor:'rgb(178,44,72)'},styles.topBar]}><Text style={[{color:'#969696'},styles.topBarText]}>
      {props.scene.route.params.title}
      </Text>
      <TouchableOpacity activeOpacity={.6} style={{position:'absolute',top:45,right:20}}
      onPress={()=> props.scene.route.params.isDrawerOpen ? props.scene.route.params.closeDrawer() : props.scene.route.params.openDrawer()}>
        <Icon name={props.scene.route.params.isDrawerOpen ? "times" : "bars"} color={"#aaaaaa"} size={RF(2.5)}/>
      </TouchableOpacity>
      <TouchableOpacity activeOpacity={.6} style={{position:'absolute',top:45,left:20}}
      onPress={()=>props.navigation.navigate('BahramanPage')}>
        <Icon name='chevron-left' color={"#aaaaaa"} size={RF(2.5)}/>
      </TouchableOpacity>
      </View>
    }
  }
);
const GlarayNavigator = createStackNavigator(
  {
    GalaryPage: {screen: Galary,params: { title: 'گالری' }},
  },
  {
    initialRouteName: 'GalaryPage',
    defaultNavigationOptions : {
      header : props => <View style={[{position:'relative',backgroundColor:'rgb(178,44,72)'},styles.topBar]}><Text style={[{color:'#969696'},styles.topBarText]}>
      {props.scene.route.params.title}
      </Text>
      <TouchableOpacity activeOpacity={.6} style={{position:'absolute',top:45,right:20}}
      onPress={()=> props.scene.route.params.isDrawerOpen ? props.scene.route.params.closeDrawer() : props.scene.route.params.openDrawer()}>
        <Icon name={props.scene.route.params.isDrawerOpen ? "times" : "bars"} color={"#aaaaaa"} size={RF(2.5)}/>
      </TouchableOpacity>
      <TouchableOpacity activeOpacity={.6} style={{position:'absolute',top:45,left:20}}
      onPress={()=>props.navigation.navigate('BahramanPage')}>
        <Icon name='chevron-left' color={"#aaaaaa"} size={RF(2.5)}/>
      </TouchableOpacity>
      </View>
    }
  }
);

const DocumentsNavigator = createStackNavigator(
  {
    DocumentsPage: {screen: Documents,params: { title: 'شناسایی انواع زعفران' }},
  },
  {
    initialRouteName: 'DocumentsPage',
    defaultNavigationOptions : {
      header : props => <View style={[{position:'relative',backgroundColor:'rgb(178,44,72)'},styles.topBar]}><Text style={[{color:'#969696'},styles.topBarText]}>
      {props.scene.route.params.title}
      </Text>
      <TouchableOpacity activeOpacity={.6} style={{position:'absolute',top:45,right:20}}
      onPress={()=> props.scene.route.params.isDrawerOpen ? props.scene.route.params.closeDrawer() : props.scene.route.params.openDrawer()}>
        <Icon name={props.scene.route.params.isDrawerOpen ? "times" : "bars"} color={"#aaaaaa"} size={RF(2.5)}/>
      </TouchableOpacity>
      <TouchableOpacity activeOpacity={.6} style={{position:'absolute',top:45,left:20}}
      onPress={()=>props.navigation.navigate('BahramanPage')}>
        <Icon name='chevron-left' color={"#aaaaaa"} size={RF(2.5)}/>
      </TouchableOpacity>
      </View>
    }
  }
);


const TabNavigator = createBottomTabNavigator({
  Bahraman: BahramanNavigator,
  Settings: CookingNavigator,
  galary: GlarayNavigator,
  documents:DocumentsNavigator

},{
  tabBarComponent: props =>
  <TabBarComponentBahraman
    {...props}
  />
},{navigationOptions: () => ({header: null})}
);

const AppContainer = createAppContainer(TabNavigator);
class App extends React.Component {
  render() {
    return (
      <AppContainer
        ref={navigatorRef => {
          NavigationService.setTopLevelNavigator(navigatorRef);
        }}
      />
    );
  }
}



styles = StyleSheet.create({
  topBarText: {
    // fontFamily:'Mj_Flow',
    fontSize:RF(2.4),
    textAlign:'center'
  },
  topBar: {
    width:'100%',
    paddingHorizontal:10,
    paddingTop:Platform.OS == "ios" ? 38 : StatusBar.currentHeight,
    paddingBottom:10
  }
})
export default App;