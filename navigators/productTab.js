import React from 'react';
import { createBottomTabNavigator, createAppContainer,createStackNavigator ,createSwitchNavigator,createMaterialTopTabNavigator} from 'react-navigation';
import Zaferan from '../pages/Bahraman/productTabs/Zaferan'
import Advie from '../pages/Bahraman/productTabs/Advie'
import Nabat from '../pages/Bahraman/productTabs/Nabat'
import other from '../pages/Bahraman/productTabs/other'
import TabBarComponentProducts from '../components/Main/TabBarComponentProducts'
import Icon from 'react-native-vector-icons/FontAwesome';
import ProductDetail from './../pages/Bahraman/ProductDetail'
import RF from 'react-native-responsive-fontsize'
import {Text,View,TouchableOpacity} from 'react-native'



const Detail1= createStackNavigator({
    zaferan :Zaferan,
    productDetail:ProductDetail, 
  },{
  headerMode: 'none',
    navigationOptions: {
        headerVisible: false,
    }},{
    initialRouteName: 'zaferan',
  }, 
)
const Detail2= createStackNavigator({
    nabat:Nabat,
    productDetail:ProductDetail, 
},{
headerMode: 'none',
  navigationOptions: {
      headerVisible: false,
  }},{
  initialRouteName: 'zaferan',
}, 
)
const Detail3= createStackNavigator({
  advie:Advie,
  productDetail:ProductDetail, 
},{
headerMode: 'none',
  navigationOptions: {
      headerVisible: false,
  }},{
  initialRouteName: 'zaferan',
}, 
)
const Detail4= createStackNavigator({
  other:other,
  productDetail:ProductDetail, 
},{
headerMode: 'none',
  navigationOptions: {
      headerVisible: false,
  }},{
  initialRouteName: 'zaferan',
}, 
)
export default productTab = createMaterialTopTabNavigator({
  detail1:Detail1,
  detail2:Detail2,
  detail3:Detail3,
  detail4:Detail4,
},
{
  tabBarComponent: props =>
  <TabBarComponentProducts
    {...props}
  />
},
{
  initialRouteName: 'BahramanPage',
  defaultNavigationOptions : {
    header : props => <View style={[{position:'relative',backgroundColor:'white'},styles.topBar]}><Text style={[{color:'#969696'},styles.topBarText]}>
    {props.scene.route.params.title}
    </Text>
    <TouchableOpacity activeOpacity={.6} style={{position:'absolute',top:45,right:20}}
    onPress={()=> props.scene.route.params.isDrawerOpen ? props.scene.route.params.closeDrawer() : props.scene.route.params.openDrawer()}>
      <Icon name={props.scene.route.params.isDrawerOpen ? "times" : "bars"} color={"#aaaaaa"} size={RF(2.5)}/>
    </TouchableOpacity>
    <TouchableOpacity activeOpacity={.6} style={{position:'absolute',top:45,left:20}}
    onPress={()=>props.navigation.navigate('BahramanPage')}>
      <Icon name='chevron-left' color={"#aaaaaa"} size={RF(2.5)}/>
    </TouchableOpacity>
    </View>
  }
},
);